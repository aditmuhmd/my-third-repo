from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('about/', views.about, name='about'),
    path('jadwal/', views.jadwal, name='jadwal'),
    path('jadwal/create', views.jadwal_create, name="jadwal_create"),
    path('jadwal/delete/<int:id>', views.jadwal_delete, name="jadwal_delete")
]
