from django.db import models
from datetime import datetime
# Create your models here.


class jadwalKegiatan (models.Model):
    aktivitas = models.CharField(max_length=100, default='Aktivitas')
    tanggal = models.DateField()
    jam = models.TimeField(default=datetime.now)
    tempat = models.CharField(max_length=100, default='Tempat')
    kategori = models.CharField(max_length=100, default='Other')

    def __str__(self):
        return self.aktivitas
