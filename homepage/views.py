from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect

from .forms import formKegiatan
from .models import jadwalKegiatan

# Create your views here.


def index(request):
    return render(request, 'index.html')


def about(request):
    return render(request, 'about.html')


def jadwal(request):
    jadwal = jadwalKegiatan.objects.all().order_by('tanggal')
    return render(request, 'jadwal.html', {'jadwalKegiatan': jadwal})


def jadwal_create(request):
    if request.method == 'POST':
        form = formKegiatan(request.POST)
        if form.is_valid():
            form.save()

            return HttpResponseRedirect('/jadwal/')
    else:
        form = formKegiatan()
    return render(request, 'jadwal_create.html', {'form': form})


def jadwal_delete(request, id):
    if request.method == 'POST':
        jadwalKegiatan.objects.filter(id=id).delete()
        return HttpResponseRedirect('/jadwal/')
    else:
        return HttpResponse("GET NOT ALLOWED")
