from django import forms
from homepage.models import jadwalKegiatan


class formKegiatan(forms.ModelForm):
    aktivitas = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'required': True, }))
    tanggal = forms.DateField(widget=forms.DateInput(attrs={'class': 'form-control', 'required': True, }))
    jam = forms.TimeField(widget=forms.TimeInput(attrs={'class': 'form-control', 'required': True, }))
    tempat = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'required': True, }))
    kategori = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'required': True, }))

    class Meta:
        model = jadwalKegiatan
        fields = ['aktivitas', 'tanggal', 'jam', 'tempat', 'kategori']
